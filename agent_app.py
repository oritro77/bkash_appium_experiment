import sys
from time import sleep
from random import randint
import json
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def print_error(e):
    print(type(e))
    print(e.args)
    print(e)

def tap_button(id, type=None, wait_time=10):
    if type is None:
        next_button = driver.find_element_by_id(id)
        next_button.click()
    else:
        #permission 2
        button = wait_till_find_element(type, id, wait_time)
        if(button is not None):
            button.click()
        else:
            print(id + " is not found")
            return False
    return True        
        

def enter_pin(type, selector_pin, pin_map):
    pin_input_field = wait_till_find_element(type, selector_pin)
    if pin_input_field is not None:
        for val in pin_map:
            element = driver.find_element_by_id(ids[val])
            element.click()
    else:
        return False
    return True    

def wait_till_find_element(type, selector, sleep=10):

    try:
        element = WebDriverWait(driver, sleep).until(
            EC.presence_of_element_located((type, selector))
        )
    except Exception as e:
        print_error(e)
        print(selector + " not found")
        return None
    else:
        return element


#start of the script
if len(sys.argv) >= 3:
    desired_caps_json = sys.argv[1]
    agent_json = sys.argv[2]
else:
    print("Please Enter the Path of Desired Capabilities and other configurations")
    exit()

desired_caps_data = None
agent_data = None
ids = {
        "permission_button": "com.android.packageinstaller:id/permission_allow_button",
        "welcome_continue": "com.bkash.businessapp.sit:id/layout_welcome_continue",
        "next_arrow":"com.bkash.businessapp.sit:id/iv_next_arrow",
        "mobile_input":"com.bkash.businessapp.sit:id/et_enter_account_number",
        "otp_input":"com.bkash.businessapp.sit:id/et_enter_otp",
        "pin_input":"com.bkash.businessapp.sit:id/et_enter_pin",
        "pin_button_1":"com.bkash.businessapp.sit:id/pinpad_button_1", 
        "pin_button_2":"com.bkash.businessapp.sit:id/pinpad_button_2", 
        "pin_button_3":"com.bkash.businessapp.sit:id/pinpad_button_3", 
        "pin_button_4":"com.bkash.businessapp.sit:id/pinpad_button_4", 
        "pin_button_5":"com.bkash.businessapp.sit:id/pinpad_button_5", 
        "pin_button_6":"com.bkash.businessapp.sit:id/pinpad_button_6", 
        "pin_button_7":"com.bkash.businessapp.sit:id/pinpad_button_7", 
        "pin_button_8":"com.bkash.businessapp.sit:id/pinpad_button_8", 
        "pin_button_9":"com.bkash.businessapp.sit:id/pinpad_button_9", 
        "pin_button_0":"com.bkash.businessapp.sit:id/pinpad_button_0",
        "term&condition_layout_next":"com.bkash.businessapp.sit:id/next_layout_arrow",
        "feature": "com.bkash.businessapp.sit:id/item_layout", 
        "feature_name": "com.bkash.businessapp.sit:id/tv_feature_name",
        "customer_mobile": "com.bkash.businessapp.sit:id/et_enter_search_content",
        "contact_next_arrow": "com.bkash.businessapp.sit:id/iv_contact_next_arrow",
        "cash_in": "com.bkash.businessapp.sit:id/et_cashin_amount",
        "cash_in_next": "com.bkash.businessapp.sit:id/iv_cashin_amount_next",
        "cash_in_pin_view": "com.bkash.businessapp.sit:id/pin_digits_view",
        "cash_in_pin_next": "com.bkash.businessapp.sit:id/iv_next_arrow",
        "tap_bkash_logo": "com.bkash.businessapp.sit:id/tap_hold_bkash_logo",
        "error_message": "com.bkash.businessapp.sit:id/tv_error_message"
    }

try:
    with open(desired_caps_json) as desired_caps_file, open(agent_json) as agent_file:
        desired_caps_data = json.load(desired_caps_file)
        agent_data = json.load(agent_file)
except IOError as e:
    print("operation failed. " + desired_caps_json + " or " + agent_file + " is not valid path")

desired_caps = desired_caps_data['capabilities'][0]
driver = webdriver.Remote(desired_caps_data['configuration']['hubHost'] + ":" \
        + desired_caps_data['configuration']['hubPort'] + "/wd/hub", desired_caps)

print(driver.current_activity)

agent_msisdn = agent_data["agentMsisdn"]
customer_msisdn = agent_data["customerMsisdn"]
cash_in_amount = str(randint(50,100))
pin = agent_data["agentPin"]
pin_map = []
for p in pin:
    pin_map.append("pin_button_" + p)

#permission 1

permission_button_found_and_clicked = tap_button(ids["permission_button"], By.ID, 2)

if(permission_button_found_and_clicked == True):
    

    #permission 2
    tap_button(ids["permission_button"], By.ID, 2)
        
    print(driver.current_activity)
    #permission 3
    tap_button(ids["permission_button"], By.ID, 10)

    print(driver.current_activity)
else:
    print("permission button not found")



# welcome screen
tap_button(ids["welcome_continue"], By.ID)
print(driver.current_activity)

# mobile number input
mobile_input_field = wait_till_find_element(By.ID, ids["mobile_input"])
if(mobile_input_field is not None):
    mobile_input_field.send_keys(agent_msisdn)
    tap_button(ids["next_arrow"])
    
else:
    print("mobile input field is not found")

print(driver.current_activity)

# otp input
otp_input = wait_till_find_element(By.ID, ids["otp_input"])
if(otp_input is not None):
    driver.implicitly_wait(10)
else:
    print("otp input field is not found")    

print(driver.current_activity)

# pin input
enter_pin(By.ID, ids["pin_input"], pin_map)
tap_button(ids["next_arrow"])
    

print(driver.current_activity)

# terms & condition
tap_button(ids["term&condition_layout_next"], By.ID)
# terms_condition_accept_button = wait_till_find_element(, )
# if(terms_condition_accept_button is not None):
#     terms_condition_accept_button.click()
# else:
#     print("terms_condition_accept_button not found")


print(driver.current_activity)

# get the features
is_feature_element_visible = wait_till_find_element(By.ID, ids["feature"])
if(is_feature_element_visible is not None):
    feature_elements = driver.find_elements_by_id(ids["feature"])
    cash_in_feature = feature_elements[0]
    cash_in_feature.click()
else:
    print("features are not found")
 
print(driver.current_activity)

customer_mobile_input = wait_till_find_element(By.ID, ids["customer_mobile"])
if(customer_mobile_input is not None):
    customer_mobile_input.send_keys(customer_msisdn)
    tap_button(ids["contact_next_arrow"])
else:
    print("pin input is not found")

cash_amount_input = wait_till_find_element(By.ID, ids["cash_in"])
if cash_amount_input is not None:
    cash_amount_input.send_keys(cash_in_amount)
    tap_button(ids["cash_in_next"])
    
else:
    print("cash amount input is not found")

enter_pin(By.ID, ids["cash_in_pin_view"], pin_map)
tap_button(ids["cash_in_pin_next"])
    
tap_bkash_logo = wait_till_find_element(By.ID, ids["tap_bkash_logo"])
if tap_bkash_logo is not None:
    actions = TouchAction(driver)
    # actions.long_press(tap_bkash_logo)
    # actions.perform()
    actions.press(tap_bkash_logo).wait(8).release().perform()

driver.quit()

